# Resource Block: Terraform Backend Configuration
terraform {
  backend "s3" {
    bucket = "aws_s3_bucket.mybucket-hb.id"  # the name of the S3 bucket that was created 
    key    = "Dev/Terraform-state"      # the path to the terraform.tfstate file stored inside the bucket
    region = "us-east-2"
  }
}
